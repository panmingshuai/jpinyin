package org.pan.jpinyin;

import java.util.Arrays;

import com.github.stuxuhai.jpinyin.ChineseHelper;
import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;

/**
 * @author panmingshuai
 * @description 
 * @Time 2018年3月15日  下午5:18:12
 *
 */
public class PinyinTest {
	public static void main(String[] args) throws PinyinException {
		/**
		 * 把字的读音转出来：
		 * PinyinFormat.WITH_TONE_MARK为[zhòng, chóng]的形式
		 * PinyinFormat.WITH_TONE_NUMBER为[zhong4, chong2]的形式
		 * PinyinFormat.WITHOUT_TONE为[zhong, chong]
		 */
		System.out.println(Arrays.toString(PinyinHelper.convertToPinyinArray('重', PinyinFormat.WITH_TONE_MARK)));
		System.out.println(Arrays.toString(PinyinHelper.convertToPinyinArray('重', PinyinFormat.WITH_TONE_NUMBER)));
		System.out.println(Arrays.toString(PinyinHelper.convertToPinyinArray('重', PinyinFormat.WITHOUT_TONE)));
		
		/**
		 * 将一段话转成拼音，“，”是每个字的分隔符，最后一个参数和上面的一样
		 */
		System.out.println(PinyinHelper.convertToPinyinString("我是你大爷", ",", PinyinFormat.WITH_TONE_MARK));
		/**
		 * 判断一个字是否是多音字
		 */
		System.out.println(PinyinHelper.hasMultiPinyin('重'));
		/**
		 * 输出一段话的首字母，例如这里的结果是：zl
		 */
		System.out.println(PinyinHelper.getShortPinyin("重量"));
		/**
		 * 将一段话中的繁体字转为简体字，这里的结果是：义义
		 */
		System.out.println(ChineseHelper.convertToSimplifiedChinese("義义"));
		/**
		 * 将一段话中的简体字转为繁体字，这里的结果是：義義
		 */
		System.out.println(ChineseHelper.convertToTraditionalChinese("義义"));
		/**
		 * 判断依据话里是否有汉字
		 */
		System.out.println(ChineseHelper.containsChinese("123我234asfs12"));
		
		
	}
}
